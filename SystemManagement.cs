﻿namespace AdoDotNet.Utility
{
    public static class SystemManagement
    {
        public static void Run()
        {
            PrintMenu();
            while (true)
            {
                int choice = (int)GetNumberInput();
                switch (choice)
                {
                    case 1:
                        ClassManagement cl = new ClassManagement();
                        cl.ManageObject();
                        break;
                    case 2:
                        StudentManagement student = new StudentManagement();
                        student.ManageObject();
                        break;
                    case 3:
                        SubjectManagement subject = new SubjectManagement();
                        subject.ManageObject();
                        break;
                    case 4:
                        ScoreManagement score = new ScoreManagement();
                        score.ManageObject();
                        break;
                    case 5:
                        Exit();
                        break;
                    default:
                        Console.WriteLine("Please Enter valid number!!");
                        break;
                }
            }
        }

        public static void PrintMenu()
        {
            Console.WriteLine("Select table to manage:");
            Console.WriteLine("1. Class\n2. Student\n3. Subject\n4. Score\n5. Exit");
        }

        /// <summary>
        /// get input number from user
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static double GetNumberInput(string msg = "Enter your choice>")
        {
            double result = 0;
            while (true)
            {
                Console.Write(msg);
                string input = Console.ReadLine()!;
                if (double.TryParse(input, out result))
                    break;
                else
                    Console.WriteLine("Please Enter valid number!!");
            }
            return result;
        }

        private static void Exit()
        {
            Environment.Exit(0);
        }
    }
}
