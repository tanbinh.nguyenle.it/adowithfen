﻿using Newtonsoft.Json;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace AdoDotNet.DataAccess
{
    public class DataAccess<T> : IDataAccess<T> where T : class
    {
        private readonly string tableName = typeof(T).Name;
        private readonly PropertyInfo[] propertyList = typeof(T).GetProperties();

        public bool Add(T obj)
        {
            string field = GetFieldOfObject();
            string param = GetParamOfObject();
            string querryString = $"Insert into {tableName} ({field}) values ({param})";
            try
            {
                using (SqlConnection connection = CreateConnection())
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(querryString, connection);
                    foreach (var prop in propertyList)
                    {
                        if (prop.Name == "ID")
                            continue;
                        cmd.Parameters.AddWithValue($"@{prop.Name}", prop.GetValue(obj));
                    }
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public List<T> ViewAll()
        {
            string querryString = $"Select * from {tableName}";
            try
            {
                using (SqlConnection connection = CreateConnection())
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = new SqlCommand(querryString, connection);

                    DataTable result = new DataTable();
                    adapter.Fill(result);
                    return ConvertDataTableToList(result);
                }
            }
            catch (Exception)
            {
                return new List<T>();
            }

        }
        public bool Update(T obj, int id)
        {
            string fieldAndParam = GetFieldAndParamOfObject();
            string querryString = $"Update {tableName} set {fieldAndParam} where ID=@ID";
            try
            {
                using (SqlConnection connection = CreateConnection())
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(querryString, connection);
                    cmd.Parameters.AddWithValue("@ID", id);
                    foreach (var prop in propertyList)
                    {
                        if (prop.Name == "ID")
                            continue;
                        cmd.Parameters.AddWithValue($"@{prop.Name}", prop.GetValue(obj));
                    }
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(int id)
        {
            string querryString = $"Delete from {tableName} where ID=@id";
            try
            {
                using (SqlConnection connection = CreateConnection())
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand(querryString, connection);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Any(int id)
        {
            string querryString = $"Select * from {tableName} where ID = @id";
            try
            {
                using (SqlConnection connection = CreateConnection())
                {
                    connection.Open();

                    SqlCommand cmd = new SqlCommand(querryString, connection);
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;

                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return dt.Rows.Count > 0;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static SqlConnection CreateConnection()
        {
            string SqlconnectionString = ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString;
            return new SqlConnection(SqlconnectionString);
        }

        /// <summary>
        /// Get field of object except ID
        /// </summary>
        /// <returns>example string: "field1,field2"</returns>
        private string GetFieldOfObject() => string.Join(", ", propertyList
            .Select(x => x.Name)
            .Where(name => name != "ID")
            .ToList());

        /// <summary>
        /// Get param of object from field except ID
        /// </summary>
        /// <returns>example string: "@filed1,@field2"</returns>
        private string GetParamOfObject() => string.Join(", ", propertyList
            .Where(prop => prop.Name != "ID")
            .Select(prop => $"@{prop.Name}"));

        /// <summary>
        /// Get field and param of object from field except ID
        /// </summary>
        /// <returns>example string: "field1=@filed1, field2=@field2"</returns>
        private string GetFieldAndParamOfObject() => string.Join(", ", propertyList
            .Where(prop => prop.Name != "ID")
            .Select(prop => $"{prop.Name}=@{prop.Name}"));

        /// <summary>
        /// Convert datatable to json then to List<T>
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        private List<T> ConvertDataTableToList(DataTable dataTable)
        {
            if (dataTable == null)
            {
                return new List<T>() { };
            }
            return JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(dataTable))!;
        }
    }
}
