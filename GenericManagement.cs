﻿using AdoDotNet.DataAccess;
using Newtonsoft.Json.Linq;

namespace AdoDotNet.Utility
{
    public abstract class GenericManagement<T> where T : class, new()
    {
        protected readonly string tableName;
        protected readonly IDataAccess<T> _db;

        protected GenericManagement()
        {
            tableName = typeof(T).Name;
            _db = new DataAccess<T>();
        }
        public virtual void ManageObject()
        {

            while (true)
            {
                PrintSubMenu();
                int choice = (int)SystemManagement.GetNumberInput();
                switch (choice)
                {
                    case 1:
                        AddObject();
                        break;
                    case 2:
                        ViewAllObject();
                        break;
                    case 3:
                        UpdateObject();
                        break;
                    case 4:
                        DeleteObject();
                        break;
                    case 5:
                        Console.WriteLine();
                        SystemManagement.PrintMenu();
                        return;
                    default:
                        Console.WriteLine("Please Enter valid option!!");
                        break;
                }
            }

        }
        public virtual void AddObject()
        {
            T obj = GetObject();
            if (_db.Add(obj))
            {
                Console.WriteLine($"\nAdd {tableName} successfully!!\n");
                return;
            }
            Console.WriteLine($"\nAdd {tableName} fail!!\n");
        }
        public virtual void UpdateObject()
        {
            Console.Write("Enter ID: ");
            int id = (int)SystemManagement.GetNumberInput("");

            if (!_db.Any(id))
            {
                Console.WriteLine($"\n{tableName} ID doesn't exists!!\n");
                return;
            }
            T obj = GetUpdateObject();
            if (_db.Update(obj, id))
            {
                Console.WriteLine($"\nUpdate {tableName} successfully!!\n");
                return;
            }
            Console.WriteLine($"\nUpdate {tableName} fail!!\n");
        }
        public virtual void ViewAllObject()
        {
            List<T> list = _db.ViewAll();
            if (list.Count == 0)
            {
                Console.WriteLine($"{tableName} doesnt have any record");
            }
            Console.WriteLine($"\n{tableName} Infomation:\n---------");
            var props = typeof(T).GetProperties();
            foreach (T obj in list)
            {
                foreach (var prop in props)
                {
                    Console.WriteLine($"{tableName} {prop.Name}: {prop.GetValue(obj)}");
                }
                Console.WriteLine("----------");
            }
            Console.WriteLine();
        }
        public virtual void DeleteObject()
        {
            Console.Write("Enter ID for delete: ");
            int id = (int)SystemManagement.GetNumberInput("");
            if (!_db.Any(id))
            {
                Console.WriteLine($"\n{tableName} ID doesn't exists!!\n");
                return;
            }
            if (_db.Delete(id))
            {
                Console.WriteLine($"\nDelete {tableName} successfully!!\n");
                return;
            }
            Console.WriteLine($"\nDelete {tableName} fail!!\n");
        }

        public T GetObject()
        {
            Console.WriteLine("Add new {0}", tableName);
            T obj = new T();

            var prop = obj.GetType().GetProperties();

            foreach (var item in prop)
            {
                if (item.Name == "ID")
                    continue;

                Console.Write($"Enter {item.Name}: ");
                if (item.PropertyType.IsValueType)
                {
                    var input = Convert.ChangeType(SystemManagement.GetNumberInput(""), item.PropertyType); //convert number from user to property Type
                    item.SetValue(obj, input);
                }
                else
                {
                    string input = Console.ReadLine()!;
                    item.SetValue(obj, input);
                }
            }
            return obj;
        }
        public T GetUpdateObject()
        {
            T obj = new T();

            var prop = obj.GetType().GetProperties();

            foreach (var item in prop)
            {
                if (item.Name == "ID")
                    continue;

                Console.Write($"Enter {item.Name}: ");
                if (item.PropertyType.IsValueType)
                {
                    var input = Convert.ChangeType(SystemManagement.GetNumberInput(""), item.PropertyType);
                    obj.GetType().GetProperty(item.Name)!.SetValue(obj, input);
                }
                else
                {
                    string input = Console.ReadLine()!;
                    obj.GetType().GetProperty(item.Name)!.SetValue(obj, input);
                }
            }
            return obj;
        }
        private void PrintSubMenu()
        {
            Console.WriteLine($"--Select your option for {tableName}: ");
            Console.WriteLine(
                    "\t1.  Add a new {0}\r\n" +
                    "\t2.  View all {0}\r\n" +
                    "\t3.  Update a {0}\r\n" +
                    "\t4.  Delete a {0}\r\n" +
                    "\t5.  Back to menu", tableName);
        }
    }
}
