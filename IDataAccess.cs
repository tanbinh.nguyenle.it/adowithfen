﻿namespace AdoDotNet.DataAccess
{
    public interface IDataAccess<T> where T : class
    {
        bool Add(T obj);
        List<T> ViewAll();
        bool Update(T obj, int id);
        bool Delete(int id);
        bool Any(int id);
    }
}
